var merge = require('webpack-merge')
var prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  URL_BASE:'http://206.81.14.204:3000/'
  //URL_BASE:'http://localhost:3000/'
})
// URL_BASE:'http://206.81.14.204:3000/'
// URL_BASE:'http://localhost:3000/'