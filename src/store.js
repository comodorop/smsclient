import Vue from 'vue'
import Vuex from 'vuex'
import http from './services/axios-http'
import config from '../config/dev.env'
Vue.use(Vuex)
const store = new Vuex.Store({
  state: {
    lstContacts: [],
    contacts: []
  },
  mutations: {
    setLstContacts (state, lstContacts) {
      state.lstContacts = lstContacts
    },
    SETCONTACTS (state, contacts) {
      state.contacts = contacts
    }
  },
  actions: {
    getContacts (context, uuidCategory) {
      console.log(uuidCategory)
      http.get(`${config.URL_BASE}v1/categories/${uuidCategory}/contacts`).then(msg => {
        context.commit('SETCONTACTS', msg.data.data)
        // this.contacts = msg.data.data
      })
    }
  }
})
export default store
