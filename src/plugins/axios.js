import axios from 'axios'
import config from '../../config/dev.env'
import app from '../main'

let band = false
axios.interceptors.request.use((config) => {
  // app.$Progress.start()
  config.headers['content-type'] = 'application/json'
  config.headers['Authorization'] = `Bearer ${localStorage.getItem('access_token')}`
  var value = config.url.split('/')
  var extencion = `${value[(value.length - 2)]}/${value[(value.length - 1)]}`
  if (extencion === 'sender/upload') {
    config.headers['content-type'] = 'multipart/form-data'
  }
  return config
}, (err) => {
  return Promise.reject(err)
})
axios.interceptors.response.use((config) => {
  // app.$Progress.finish()
  return config
},
error => {
  const originalRequest = error.config
  if (error.response.status === 401 && !band) {
    band = true
    return axios.post(`${config.URL_BASE}auth/refresh`, { grant_type: 'refresh_token', refresh_token: localStorage.getItem('refresh_token') }).then((msg) => {
      if (msg.data.data.error === 'No se puede actualizar el token. Puede que ya no exista o haya expirado.') {
        window.localStorage.removeItem('access_token')
        window.localStorage.removeItem('refresh_token')
        app.$router.push('/')
      }
      window.localStorage.setItem('access_token', msg.data.data.access_token)
      window.localStorage.setItem('refresh_token', msg.data.data.refresh_token)
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + msg.data.access_token
      originalRequest.headers['Authorization'] = 'Bearer ' + msg.data.access_token
      band = false
      return axios(originalRequest)
    }, _ => {
      window.localStorage.removeItem('access_token')
      window.localStorage.removeItem('refresh_token')
      app.$router.push('/')
    })
  }
  band = !band
  return Promise.reject(error)
})

export default {
  install: function (Vue, name = '$http') {
    Object.defineProperty(Vue.prototype, name, { value: axios })
  }
}
