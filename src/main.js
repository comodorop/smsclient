import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import App from './App'
import router from './router'
// import axios from 'axios'
import VueAxios from 'vue-axios'
import JsonExcel from 'vue-json-excel'
import bModal from 'bootstrap-vue/es/components/modal/modal'
import bModalDirective from 'bootstrap-vue/es/directives/modal/modal'
import { Modal } from 'bootstrap-vue/es/components'
import { Notifications } from './services/notifications'
import vSelect from 'vue-select'
import Vue2Filters from 'vue2-filters'
import config from '../config/dev.env'
import sweetAlert from 'vue-sweetalert2'
import VeeValidate from 'vee-validate'
import store from './store'
import VueTheMask from 'vue-the-mask'
import { Datetime } from 'vue-datetime'
import AxiosPlugin from './plugins/axios'
import EventBus from './plugins/event-bus'
import VueSocketio from 'vue-socket.io'
import VueStar from 'vue-star'
import VueHowler from 'vue-howler'

Vue.component('star', VueStar)
Vue.use(VueHowler)
Vue.use(VueSocketio, `${config.URL_BASE}`)
Vue.use(EventBus)
Vue.use(require('vue-moment'))
Vue.component('v-select', vSelect)
Vue.use(Notifications)
Vue.component('dowloadExcel', JsonExcel)
Vue.use(BootstrapVue, VueAxios)
Vue.use(Vue2Filters)
Vue.component('b-modal', bModal)
Vue.directive('b-modal', bModalDirective)
Vue.use(Modal)
Vue.use(sweetAlert)
Vue.use(VueTheMask)
Vue.use(Datetime)
Vue.component('datetime', Datetime)
Vue.use(AxiosPlugin, '$axios')
Vue.use(VeeValidate, {
  inject: true,
  fieldsBackName: 'veeFields'
})

// let band = false
// axios.interceptors.request.use((config) => {
//   config.headers['content-type'] = 'application/json'
//   config.headers['Authorization'] = `Bearer ${localStorage.getItem('access_token')}`
//   var value = config.url.split('/')
//   var extencion = `${value[(value.length - 2)]}/${value[(value.length - 1)]}`
//   if (extencion === 'sender/upload') {
//     config.headers['content-type'] = 'multipart/form-data'
//   }
//   return config
// }, (err) => {
//   return Promise.reject(err)
// })
// axios.interceptors.response.use((config) => {
//   return config
// },
// error => {
//   const originalRequest = error.config
//   if (error.response.status === 401 && !band) {
//     band = true
//     return axios.post(`${config.URL_BASE}auth/refresh`, { grant_type: 'refresh_token', refresh_token: localStorage.getItem('refresh_token') }).then((msg) => {
//       console.log(msg)
//       window.localStorage.setItem('access_token', msg.data.data.access_token)
//       window.localStorage.setItem('refresh_token', msg.data.data.refresh_token)
//       axios.defaults.headers.common['Authorization'] = 'Bearer ' + msg.data.access_token
//       originalRequest.headers['Authorization'] = 'Bearer ' + msg.data.access_token
//       band = false
//       return axios(originalRequest)
//     }, error => {
//       console.log(error)
//       window.localStorage.removeItem('access_token')
//       window.localStorage.removeItem('refresh_token')
//       router.push('/')
//     })
//   }
//   band = !band
//   return Promise.reject(error)
// })

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: {
    App
  },
  store
})
