import Vue from 'vue'
import VueNotifications from 'vue-notifications'
import iziToast from 'izitoast'// https://github.com/dolce/iziToast
import 'izitoast/dist/css/iziToast.min.css'

function toast ({title, message, type, timeout, cb}) {
  if (type === VueNotifications.types.warn) type = 'warning'
  return iziToast[type]({title, message, timeout})
}

const options = {
  success: toast,
  error: toast,
  info: toast,
  warn: toast
}

Vue.use(VueNotifications, options)

export const Notifications = {
  install (Vue, options) {
    Vue.prototype.$notif = new Vue({
      notifications: {
        showSuccessMsg: {
          type: VueNotifications.types.success,
          title: 'Exito',
          message: 'That\'s the success!'
        },
        showInfoMsg: {
          type: VueNotifications.types.info,
          title: 'Hey you',
          message: 'Here is some info for you'
        },
        showWarnMsg: {
          type: VueNotifications.types.warn,
          title: 'Wow, man',
          message: 'That\'s the kind of warning'
        },
        showErrorMsg: {
          type: VueNotifications.types.error,
          title: 'Wow-wow',
          message: 'That\'s the error'
        }
      }
    })
  }
}
