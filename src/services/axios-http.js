import axios from 'axios'
// axios.defaults.headers.common['content-type'] = 'application/json'
function post (url, data) {
  var promise = new Promise((resolve, reject) => {
    axios({
      method: 'post',
      url: url,
      data: data
    }).then((msg) => {
      resolve(msg)
    }).catch((err) => {
      reject(err)
    })
  })
  return promise
}
function get (url) {
  var promise = new Promise((resolve, reject) => {
    axios({
      method: 'get',
      url: url,
      headers: {
        Authorization: `Bearer ${localStorage.getItem('access_token')}`
      }
    }).then((msg) => {
      resolve(msg)
    }).catch((err) => {
      console.log(err)
      reject(err)
    })
  })
  return promise
}

function deleteData (url, data) {
  var promise = new Promise((resolve, reject) => {
    axios({
      method: 'delete',
      url: url,
      data: data
    }).then((msg) => {
      resolve(msg)
    }).catch((err) => {
      console.log(err)
      reject(err)
    })
  })
  return promise
}

function patchData (url, data) {
  var promise = new Promise((resolve, reject) => {
    axios({
      method: 'patch',
      url: url,
      data: data
    }).then((msg) => {
      resolve(msg)
    }).catch((err) => {
      reject(err)
    })
  })
  return promise
}

function putData (url, data) {
  var promise = new Promise((resolve, reject) => {
    axios({
      method: 'put',
      url: url,
      data: data
    }).then((msg) => {
      resolve(msg)
    }).catch((err) => {
      console.log(err)
      reject(err)
    })
  })
  return promise
}

function refreshToken () {
  console.log('Va a refrescar el token')
  var promise = new Promise((resolve, reject) => {
    axios({
      method: 'post',
      url: 'http://localhost:3000/auth/refresh',
      data: {
        grant_type: 'refresh_token',
        refresh_token: localStorage.getItem('refresh_token')
      }
    }).then(msg => {
      console.log('*************************')
      console.log(msg)
      console.log('*************************')
      localStorage.removeItem('access_token')
      localStorage.removeItem('refresh_token')
      localStorage.setItem('access_token', msg.data.access_token)
      localStorage.setItem('refresh_token', msg.data.refresh_token)
      resolve(1)
    }).catch((err) => {
      console.log('marco un error')
      reject(err)
      console.log(err)
    })
  })
  return promise
}

export default {
  post,
  get,
  refreshToken,
  deleteData,
  putData,
  patchData
}
