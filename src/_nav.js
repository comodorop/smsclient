export default {
  items: [
    {
      name: 'Inicio',
      url: '/main',
      icon: 'fa fa-home',
      rol: 'admin'
    },
    {
      name: 'Inicio',
      url: '/main',
      icon: 'fa fa-home',
      rol: 'normal'
    },
    {
      name: 'Contactos ',
      url: '/contact',
      icon: 'fa fa-group',
      rol: 'admin',
      children: [
        {
          name: 'Altas',
          url: '/newContact',
          icon: 'fa fa-flickr'
        },
        {
          name: 'Lista',
          url: '/lstContact',
          icon: 'icon-user'
        }
      ]
    },
    {
      name: 'Contactos ',
      url: '/contact',
      icon: 'fa fa-group',
      rol: 'normal',
      children: [
        {
          name: 'Altas',
          url: '/newContact',
          icon: 'fa fa-flickr'
        },
        {
          name: 'Lista',
          url: '/lstContact',
          icon: 'icon-user'
        }
      ]
    },
    {
      name: 'Categorias',
      url: '/category',
      icon: 'fa fa-address-book-o',
      rol: 'admin',
      children: [
        {
          name: 'Altas',
          url: '/newCategory',
          icon: 'fa fa-user'
        },
        {
          name: 'Lista',
          url: '/lstCategories',
          icon: 'fa fa-list'
        },
        {
          name: 'Asignar Contactos',
          url: '/setCategory',
          icon: 'fa fa-address-book-o'
        }
      ]
    },
    {
      name: 'Envios',
      url: '/file',
      icon: 'fa fa-send',
      rol: 'admin',
      children: [
        {
          name: 'Archivos',
          url: '/sendFile',
          icon: 'fa fa-file-excel-o'
        },
        {
          name: 'Contactos por categoria',
          url: '/sendByCategory',
          icon: 'fa fa-address-book-o'
        }
      ]
    },
    {
      name: 'Envios',
      url: '/file',
      icon: 'fa fa-send',
      rol: 'normal',
      children: [
        {
          name: 'Archivos',
          url: '/sendFile',
          icon: 'fa fa-file-excel-o'
        },
        {
          name: 'Contactos por categoria',
          url: '/sendByCategory',
          icon: 'fa fa-address-book-o'
        }
      ]
    },
    {
      name: 'Mensajes',
      url: 'message',
      icon: 'fa fa-envelope-open-o',
      rol: 'admin',
      children: [
        {
          name: 'Lista',
          icon: 'fa fa-list',
          url: '/listMsg'
        }
      ]
    },
    {
      name: 'Usuarios',
      url: '/user',
      icon: 'fa fa-address-book',
      rol: 'admin',
      children: [
        {
          name: 'Usuarios',
          url: '/user',
          icon: 'fa fa-user'
        },
        {
          name: 'Lista de usuarios',
          url: '/lstUser',
          icon: 'fa fa-list'
        }
      ]
    },
    {
      name: 'Cerrar Sesion',
      url: '/',
      icon: 'fa fa-sign-out',
      rol: 'admin'
    },
    {
      name: 'Cerrar Sesion',
      url: '/',
      icon: 'fa fa-sign-out',
      rol: 'normal'
    }

  ]
}
