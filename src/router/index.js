import Vue from 'vue'
import Router from 'vue-router'

// Containers
import Full from '@/containers/Full'

// Views
import Dashboard from '@/views/Dashboard'
import Login from '@/views/pages/Login'
import Contact from '@/views/contacts/contacts'
import ListaContact from '@/views/contacts/lstContacts'
import Category from '@/views/categories/category'
import LstCategories from '@/views/categories/lstCategories'
import ContactCategory from '@/views/categories/ContactCategory'
import AssignContact from '@/views/categories/assignContact'
import Files from '@/views/send/sendFile'
import SendByCategory from '@/views/send/sendByCategory'
import Company from '@/views/company/company'
import User from '@/views/users/user'
import LstUsers from '@/views/users/lstUsers'
import LstMessage from '@/views/messages/lstMessage'
import DetailMessages from '@/views/messages/detailMsg'

Vue.use(Router)

export default new Router({
  mode: 'hash',

  linkActiveClass: 'open active',
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    {
      path: '/main',
      name: 'SMS',
      component: Full,
      children: [
        {
          path: '/',
          name: 'Inicio',
          component: Dashboard
        },
        {
          path: '/newContact',
          name: 'NUevo contactoss',
          component: Contact
        },
        {
          path: '/lstContact',
          name: 'Lista de contactos',
          component: ListaContact
        },
        {
          path: '/updateContact/:uuid',
          name: 'Actualizar Contactos',
          component: Contact
        },
        {
          path: '/newCategory',
          name: 'Nueva categoria',
          component: Category
        },
        {
          path: '/updateCategori/:uuidCategory',
          name: 'Actualzar categoria',
          component: Category
        },
        {
          path: '/lstCategories',
          name: 'lista de categorias',
          component: LstCategories
        },
        {
          path: '/setCategory',
          name: 'Asignar contactos',
          component: ContactCategory
        },
        {
          path: '/sendFile',
          name: 'Envio por archivo',
          component: Files
        },
        {
          path: '/sendByCategory',
          name: 'Envio por categorias',
          component: SendByCategory
        },
        {
          path: '/assignContact/:uuidCategory',
          name: 'Asignacion de contactos',
          component: AssignContact
        },
        {
          path: '/company',
          name: 'Nueva compania',
          component: Company
        },
        {
          path: '/user',
          name: 'Nuevo usuario',
          component: User
        },
        {
          path: '/lstUser',
          name: 'Lista de usuarios',
          component: LstUsers
        },
        {
          path: '/user/:uuidUser',
          name: 'Actualizar usuario',
          component: User
        },
        {
          path: '/listMsg',
          name: 'Lista de mensajes enviados',
          component: LstMessage
        },
        {
          path: '/detailMsg/:uuid',
          name: 'Detalle',
          component: DetailMessages
        }
      ]
    },
    {
      path: '/',
      component: Login
    }
  ]
})
